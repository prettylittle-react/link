import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';
//import Text from 'text';
import Icon from 'icon';
import IconExternal from './external.svg';

import {getModifiers} from 'libs/component';

// import * as Analytics from 'analytics';

import './Link.scss';

/**
 * Link
 * @description [Description]
 * @example
  <div id="Link"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Link, {
    	title : 'Example Link'
    }), document.getElementById("Link"));
  </script>
 */
class Link extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = props.baseClass;
	}

	// @Analytics.event
	onClick = ev => {
		const {onClick} = this.props;

		if (onClick) {
			onClick(ev);
		}
	};

	renderContent() {
		const {label, isExternal, icon, iconPrefix, iconSuffix} = this.props;

		if (!label) {
			return null;
		}

		const prefix = icon || iconPrefix;
		const suffix = (isExternal ? IconExternal : null) || iconSuffix;

		return (
			<React.Fragment>
				{prefix && <Icon icon={prefix} />}
				<Text content={label} />
				{suffix && <Icon icon={suffix} />}
			</React.Fragment>
		);
	}

	get atts() {
		const {href, target, role, className, isWide, style, size, rel} = this.props;

		const atts = {
			rel,
			href,
			className: getModifiers(this.baseClass, [style, isWide ? 'wide' : null, size])
		};

		if (className) {
			atts.className += ` ${className}`;
		}

		if (role) {
			atts.role = role;
		}

		atts.onClick = this.onClick;

		if (target) {
			atts.target = target;
			if (target === '_blank') {
				atts.rel = (atts.rel ? ' ' : '') + 'noopener noreferrer';
			}
		}

		atts.className = atts.className || undefined;

		return atts;
	}

	render() {
		const content = this.renderContent();

		if (!content) {
			return null;
		}

		return <a {...this.atts}>{content}</a>;
	}
}

Link.defaultProps = {
	style: null,
	size: null,
	baseClass: 'link',
	icon: null,
	label: null,
	href: null,
	className: '',
	target: '',
	role: '',
	rel: undefined,
	onClick: null,
	isExternal: false,
	isWide: false,
	iconPrefix: null,
	iconSuffix: null
};

Link.propTypes = {
	style: PropTypes.string,
	size: PropTypes.string,
	baseClass: PropTypes.string,
	icon: PropTypes.any,
	href: PropTypes.string,
	className: PropTypes.string,
	target: PropTypes.string,
	role: PropTypes.string,
	rel: PropTypes.string,
	onClick: PropTypes.func,
	isExternal: PropTypes.bool,
	isWide: PropTypes.bool,
	iconPrefix: PropTypes.any,
	iconSuffix: PropTypes.any
};

export default Link;
